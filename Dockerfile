FROM openjdk:17

ARG JAR_FILE=target/*.jar

COPY ${JAR_FILE} serviceregistry.jar

ENTRYPOINT ["java","-jar","/serviceregistry.jar"]

EXPOSE 8762

# 1. mvn clean install => Buat JAR file didalm folder target
# 2. docker build -t [namaId DockerHub]/[nama aplikasi]:[tag image (versi aplikasi)] .
# exm. docker build -t syafri/serviceregistry:0.0.1 .
# 3. docker images check docker images